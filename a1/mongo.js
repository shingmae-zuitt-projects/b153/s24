/*
1. Using Robo3T, in our hotel database, create a "rooms collection" and insert a single document with the following details:
	name - single
	accommodates - 2
	price - 1000
	description - "A simple room with all the basic necessities"
	rooms_available - 10
	isAvailable - false

2. Insert multiple rooms with the following details:
	name - double
	accommodates - 3
	price - 2000
	description - "A room fit for a small family going on a vacation"
	rooms_available - 5
	isAvailable - false

	name - queen
	accommodates - 4
	price - 4000
	description - "A room with a queen sized bed perfect for a simple getaway"
	rooms_available - 15
	isAvailable - false

3. Find rooms with a price greater than or equal to 2000

4. Find a room with a price greater than or equal to 2000 and with rooms_available greater than 10

5. Update the "queen" room to have rooms_available to 0

6. Update all rooms with rooms_available greater than 0 and the isAvailable equal to false and set the availability of these rooms to true

7. Delete all rooms with rooms_available equal to 0

8. Find a room with the name "double" and project only the following fields:

	name
	accommodates
	price
	description

*/


//NO. 1
db.users.insertOne({
		name: "single",
		accommodates : 2,
		price : 1000,
		description : "A simple room with all the basic necessities",
		rooms_available : 10,
		isAvailable : false
})


/*
result

{
    "acknowledged" : true,
    "insertedId" : ObjectId("6201062a12fd689bd99f90a3")
}

*/


//NO. 2
db.users.insertMany([
	{
		name : "double",
		accommodates : 3,
		price : 2000,
		description : "A room fit for a small family going on a vacation",
		rooms_available : 5,
		isAvailable : false
	},
	{
		name : "queen",
		accommodates : 4,
		price : 4000,
		description : "A room with a queen sized bed perfect for a simple getaway",
		rooms_available : 15,
		isAvailable : false
	}
])

/*
result

{
    "acknowledged" : true,
    "insertedIds" : [ 
        ObjectId("6201069e12fd689bd99f90a4"), 
        ObjectId("6201069e12fd689bd99f90a5")
    ]
}

*/

//NO.3
db.users.find({
	price: {
		$gte: 2000
	}
})


//result
/*1*/

/*
{
    "_id" : ObjectId("6201069e12fd689bd99f90a4"),
    "name" : "double",
    "accommodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_available" : 5.0,
    "isAvailable" : false
}
*/

/* 2 */

/*

{
    "_id" : ObjectId("6201069e12fd689bd99f90a5"),
    "name" : "queen",
    "accommodates" : 4.0,
    "price" : 4000.0,
    "description" : "A room with a queen sized bed perfect for a simple getaway",
    "rooms_available" : 15.0,
    "isAvailable" : false
}

*/

//NO.4
db.users.find({
	price: {
		$gte: 2000
	},
	rooms_available: {
		$gte: 10
	}
})


//result

/* 1 */

/*
{
    "_id" : ObjectId("6201069e12fd689bd99f90a5"),
    "name" : "queen",
    "accommodates" : 4.0,
    "price" : 4000.0,
    "description" : "A room with a queen sized bed perfect for a simple getaway",
    "rooms_available" : 15.0,
    "isAvailable" : false
}
*/


//NO. 5
db.users.updateOne(
	{
	_id: ObjectId("6201069e12fd689bd99f90a5")
	},
	{
		$set: {
			rooms_available: 0
		}
	}
)


/*

result after update:

{
    "_id" : ObjectId("6201069e12fd689bd99f90a5"),
    "name" : "queen",
    "accommodates" : 4.0,
    "price" : 4000.0,
    "description" : "A room with a queen sized bed perfect for a simple getaway",
    "rooms_available" : 0.0,
    "isAvailable" : false
}

*/

//NO. 6
db.users.updateMany(
	{
		rooms_available: {
				$gt: 0
			}
	},
	{
		$set: {
		isAvailable: true
		}
	}
	)
	

//result

/* 1 */
/*
{
    "_id" : ObjectId("6201062a12fd689bd99f90a3"),
    "name" : "single",
    "accommodates" : 2.0,
    "price" : 1000.0,
    "description" : "A simple room with all the basic necessities",
    "rooms_available" : 10.0,
    "isAvailable" : true
}
*/

/* 2 */
/*
{
    "_id" : ObjectId("62010d0712fd689bd99f90a6"),
    "name" : "double",
    "accommodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation",
    "rooms_available" : 5.0,
    "isAvailable" : true
}
*/

/* 3 */
/*
{
    "_id" : ObjectId("6201069e12fd689bd99f90a5"),
    "name" : "queen",
    "accommodates" : 4.0,
    "price" : 4000.0,
    "description" : "A room with a queen sized bed perfect for a simple getaway",
    "rooms_available" : 0.0,
    "isAvailable" : false
}
*/

//NO.7
db.users.deleteOne(
	{
		rooms_available:
		{
			$lte: 0
		}
	}
)

/*result:
{
    "acknowledged" : true,
    "deletedCount" : 1.0
}
*/


//NO.8
db.users.find(
	{
		name: "double"
	},
	{
		_id: 0,
		rooms_available:0,
		isAvailable: 0,
	}

)

/*
result
{
    "name" : "double",
    "accommodates" : 3.0,
    "price" : 2000.0,
    "description" : "A room fit for a small family going on a vacation"
}
*/