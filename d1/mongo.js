//adds skills
db.users.updateOne(
    {
        _id: ObjectId("61fbccb8bb9a0c0372eff3b0")
    },
    {
        $push: {
            skills: "HTML"
        }
    }
)

//Adds an element to the array
db.users.updateOne(
    {
        _id: ObjectId("61fbccb8bb9a0c0372eff3b0")
    },
    {
        $push: {
            skills: {
                $each: ["CSS", "JavaScript"]
            }
        }
    }
)

//Delete
db.users.updateOne(
    {
        _id: ObjectId("61fbccb8bb9a0c0372eff3b0")
    },
    {
        $unset: {
            skills: ""

        }
    }
)


//Mini Activity
db.users.insertMany([
    {User1: "Chichi", age: 26, skills: ["HTML", "CSS", "Bootstrap"] },
    {User2: "Gihan", age: 25, skills: ["HTML", "CSS", "JavaScript"] },
    {User3: "Goku", age: 31, skills: ["HTML", "MERN"] },
    {User4: "Vegeta", age: 35, skills: ["HTML", "PHP", "MySQL"] },
    {User5: "Bulma", age: 35, skills: ["JavaScript", "Python"] }
])

//search for users that know either JavaScript or MERN

db.users.find({
    skills: {
        $in: ["JavaScript", "MERN"]
    }
})

//Search for users that know HTML and CSS
db.users.find({
    skills: {
        $all: ["HTML", "CSS"]
    }
})

//MONGODB QUERY OPERATIONS & FIELD PROJECTION

//$set is a MongoDB query that replaces the value 

//Show listings that are of property type "condominium", has a minimum stay of 1 night, has 1 bedroom, and can accommodate 2 people

db.listingsAndReviews.find(
    {
        property_type: "Condominium",
        minimum_nights: "2",
        bedrooms: 1,
        accommodates: 2
    },
    {
        name: 1,
        property_type: 1,
        minimum_nights: 1,
        bedrooms: 1,
        accommodates: 2
    }
)

db.listingsAndReviews.find(
    {
        property_type: "Condominium",
        minimum_nights: "2",
        bedrooms: 1,
        accommodates: 2
    },
    {
        name: 0
    }
)

//Limit only to number you input
db.listingsAndReviews.find(
    {
        property_type: "Condominium",
        minimum_nights: "2",
        bedrooms: 1,
        accommodates: 2
    },
    {
        name: 1,
        property_type: 1,
        minimum_nights: 1,
        bedrooms: 1,
        accommodates: 2
    }
)
.limit(5)

//Show listings that have a price of less than 100, fee for additional heads that is less than 20, and are in the country Portugal

db.listingsAndReviews.find(
    {
        price: {
            $lt: 100,
            $gt: 50
        },
        extra_people: {
            $lt: 20
        },
        "address.country": "Portugal"
    },
    {
        name: 1,
        price: 1,
        extra_people: 1,
        "address.country": 1
    }
)
.limit(5)

//search for the first 5 listings by name in ascending order that have a price less than 100

db.listingsAndReviews.find(
    {
        price:{
            $lt: 100,
        }
    },
    {
        name: 1,
        price:1
    }
)
.limit(5)
.sort({
    name: 1
})

/*
    Result

    "_id" : "11012484",
    "name" : "",
    "price" : NumberDecimal("40.00")
}


{
    "_id" : "22210558",
    "name" : "\"Hôtel\" room Frontenac metro, private & furnished",
    "price" : NumberDecimal("40.00")
}


{
    "_id" : "21568012",
    "name" : "$29 per night, near city,free wifi, own room,WOW!!",
    "price" : NumberDecimal("45.00")
}


{
    "_id" : "20358824",
    "name" : "$45ANYCCozy Room with curtain NearJ,G, and M train",
    "price" : NumberDecimal("45.00")
}


{
    "_id" : "21675318",
    "name" : "( ͡° ͜ʖ ͡°) Baaasic Spaaace (ฅ´ω`ฅ)",
    "price" : NumberDecimal("79.00")
}

Increase the price of the previous 5 listings by 10

*/

db.listingsAndReviews.updateMany(
    {
        price: {
            $lt: 100
        }
    },
    {
        $inc: {
            price: 10
        }
    }
)
.limit(5)
.sort({
    name: 1
})



//use -1 for descending order
db.listingsAndReviews.find(
    {
        price:{
            $lt: 100
        }
    },
    {
        name: 1,
        price:1
    }
)
.limit(5)
.sort({
    name: -1
})



// delete listings with any one of the following conditions:
// review scores accuracy <=75
// review scores cleanliness <= 80
// review scores checkin <= 70
// review scores communication <= 70
// review scores location <= 75
// review scores value <= 80
db.listingsAndReviews.deleteMany(
    {
      $or:
      [ 
        {"review_scores.review_scores_accuracy": {$lte: 75}},
        {"review_scores.review_scores_cleanliness": {$lte: 80}},
        {"review_scores.review_scores_checkin": {$lte: 70}},
        {"review_scores.review_scores_communication": {$lte: 70}},
        {"review_scores.review_scores_location": {$lte: 75}},
        {"review_scores.review_scores_value": {$lte: 80}}
      ]
    }
  )

  //$gt  and $gte can be used for greater than and greater than or eual to,respectively

























  //SIR'S DISCUSSION
  //SIR'S DISCUSSION
  //SIR'S DISCUSSION
  //SIR'S DISCUSSION
  //SIR'S DISCUSSION
  //SIR'S DISCUSSION
  //SIR'S DISCUSSION

  //MONGODB QUERY OPERATORS & FIELD PROJECTION


//$set is a MongoDB query operator that replaces the value of a field with the specified value.

//If a given field does not yet exist in the record, $set will add a new field with the specified value.

db.users.updateOne(
	{
		_id: ObjectId("61fbcaa53a71e5359242e52f")
	},
	{
		$set: {
			skills: []
		}
	}
)

//add to an existing array

db.users.updateOne(
	{
		_id: ObjectId("61fbcaa53a71e5359242e52f")
	},
	{
		$push: {
			skills: "HTML"
		}
	}
)

//add multiple elements to an existing array at the same time

db.users.updateOne(
	{
		_id: ObjectId("61fbcaa53a71e5359242e52f")
	},
	{
		$push: {
			skills: {
				$each: ["CSS", "JavaScript"]
			}
		}
	}
)

//delete an existing field:

db.users.updateOne(
	{
		_id: ObjectId("61fbcaa53a71e5359242e52f")
	},
	{
		$unset: {
			skills: ""
		}
	}
)

/*

Mini Activity:
In ONE command, create five new users, all with a name, age, isActive field, and skills array. (info besides skills is up to you)

For each user, give the following list of skills:

User 1: HTML, CSS, Bootstrap
User 2: HTML, CSS, JavaScript
User 3: HTML, MERN
User 4: HTML, PHP, MySQL
User 5: JavaScript, Python

*/

db.users.insertMany([
	{
		name: "User 1",
		age: 30,
		isActive: true,
		skills: ["HTML", "CSS", "Bootstrap"]
	},
	{
		name: "User 2",
		age: 30,
		isActive: true,
		skills: ["HTML", "CSS", "JavaScript"]
	},
	{
		name: "User 3",
		age: 30,
		isActive: true,
		skills: ["HTML", "MERN"]
	},
	{
		name: "User 4",
		age: 30,
		isActive: true,
		skills: ["HTML", "PHP", "MySQL"]
	},
	{
		name: "User 5",
		age: 30,
		isActive: true,
		skills: ["JavaScript", "Python"]
	},
])

//search for users that know either JavaScript or MERN

db.users.find({
	skills: {
		$in: ["JavaScript", "MERN"]
	}
})

//search for users that know HTML AND CSS

db.users.find({
	skills: {
		$all: ["HTML", "CSS"]
	}
})

//Show listings that are of property type "condominium," has a minimum stay of 1 night, has 1 bedroom, and can accommodate 2 people

db.listingsAndReviews.find(
	{
		property_type: "Condominium",
		minimum_nights: "2",
		bedrooms: 1,
		accommodates: 2
	},
	{
		name: 1,
		property_type: 1,
		minimum_nights: 1,
		bedrooms: 1,
		accommodates: 1
	}
)
.limit(5)

//Show listings that have a price of less than 100, fee for additional heads that is less than 20, and are in the country Portugal

db.listingsAndReviews.find(
	{
		price: {
			$lt: 100
		},
		extra_people: {
			$lt: 20
		},
		"address.country": "Portugal"
	},
	{
		name: 1,
		price: 1,
		extra_people: 1,
		"address.country": 1
	}
)
.limit(5)

//search for the first 5 listings by name in ascending order that have a price less than 100

db.listingsAndReviews.find(
	{
		price: {
			$lt: 100
		}
	},
	{
		name: 1,
		price: 1,
	}
)
.limit(5)
.sort({
	name: 1
})

//use -1 for descending order

db.listingsAndReviews.updateMany(
	{
		price: {
			$lt: 100
		}
	},
	{
		$inc: {
			price: -10
		}
	}
)

//use a negative number to decrease instead of increase

// delete listings with any one of the following conditions:
// review scores accuracy <=75
// review scores cleanliness <= 80
// review scores checkin <= 70
// review scores communication <= 70
// review scores location <= 75
// review scores value <= 80

db.listingsAndReviews.deleteMany(
  {
    $or:
    [ 
      {"review_scores.review_scores_accuracy": {$lte: 75}},
      {"review_scores.review_scores_cleanliness": {$lte: 80}},
      {"review_scores.review_scores_checkin": {$lte: 70}},
      {"review_scores.review_scores_communication": {$lte: 70}},
      {"review_scores.review_scores_location": {$lte: 75}},
      {"review_scores.review_scores_value": {$lte: 80}}
    ]
  }
)

//$gt and $gte can be used for greater than and greater than or equal to, respectively
